import asyncio
from PIL import Image
from pathlib import Path
from bs4 import BeautifulSoup


p = Path(__file__).parent


input_path = p / 'input1'
out_dir_name = '1'
img_path_prefix = ''  # для csv вида img_path_prefix/img.jpg
out_dir = p / 'output' / out_dir_name
csv = out_dir / 'dataset.csv'
replace_pairs = {
    'micronucleus': 'micronucl',
    '2nucll': '2nucl',
    # TODO: добавить сюда оставшиеся
}


out_dir.mkdir(parents=True, exist_ok=True)
csv_io = csv.open('w')
csv_lock = asyncio.Lock()
loop = asyncio.get_event_loop()


def normalize_name(name):
    for old_name, new_name in replace_pairs.items():
        name = name.replace(old_name, new_name)
    return name


class DataPicture:
    def __init__(self, path):
        self.name = path
        self.xml_path = input_path / (path + '.xml')
        self.pic_path = input_path / (path + '.jpg')
        self.objects = []

    def parse_xml(self):
        with self.xml_path.open() as xml_file:
            soup = BeautifulSoup(xml_file.read(), 'lxml-xml')
            for obj in soup.select('annotation > object'):
                bndbox = obj.find('bndbox')
                x1, x2, y1, y2 = map(lambda x: int(bndbox.find(x).string),
                                     ['xmin', 'xmax', 'ymin', 'ymax'])

                class_ = normalize_name(obj.find('name').string)

                self.objects.append([img_path_prefix + str(self.pic_path.name),
                                     x1, y1, x2, y2,
                                     class_])

    async def save_csv(self):
        for obj in self.objects:
            for i in range(1, 4 + 1):
                obj[i] = str(int(obj[i]))

        async with csv_lock:
            for obj in self.objects:
                csv_io.write(','.join(obj) + '\n')

    def convert_img(self):
        with Image.open(self.pic_path) as img:
            w_old = img.width
            img.thumbnail([520, 520])
            scaling_factor = img.width / w_old
            img.save(out_dir / (self.name + '.jpg'), format='JPEG')

        for obj in self.objects:
            for i in range(1, 4 + 1):  # obj.index(<x1>) == 1
                obj[i] *= scaling_factor

    async def run(self):
        self.parse_xml()
        self.convert_img()
        await self.save_csv()

    def __repr__(self):
        return repr(vars(self))


async def main():
    tasks = []
    for file in [str(x.stem) for x in input_path.iterdir() if x.suffix == '.xml']:
        if not (input_path / (file + '.jpg')).exists():
            print(input_path / (file + '.jpg'), 'не найден!')
            continue
        datapic = DataPicture(file)
        tasks.append(asyncio.ensure_future(datapic.run()))

    await asyncio.gather(*tasks)

if __name__ == '__main__':
    loop.run_until_complete(main())
    csv_io.close()
