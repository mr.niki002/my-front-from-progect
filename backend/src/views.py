from tempfile import NamedTemporaryFile
from flask import request, jsonify, send_from_directory, after_this_request
from . import cfg
from .cfg import app
from .neural import predict
from pathlib import Path
import traceback
import asyncio
import sqlite3
import random
import datetime
import time
import string
def insert_data():
    conn = sqlite3.connect("users.db")
    c = conn.cursor()
    c.execute("SELECT*FROM uSeRs")
    row = c.fetchall()
    return row
def data_entry(normal, ubnormal,micro,procent):
    conn = sqlite3.connect("users.db")
    c = conn.cursor()
    # c.execute("CREATE TABLE IF NOT EXISTS uSeRs(pacient_id REAL, date TEXT, normal REAL, ubnormal REAL,micro REAL, procent REAL)")
    unix= time.time()
    pacient_id = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
    date = str(datetime.datetime.fromtimestamp(unix).strftime("%Y-%m-%d %H:%M:%S"))
    c.execute("INSERT INTO uSeRs(pacient_id,date,normal,ubnormal,micro,procent) VALUES(?,?,?,?,?,?)",(pacient_id,date,normal,ubnormal,micro,procent))
    conn.commit()
    c.close()
    conn.close()
    return pacient_id   

 
@app.route('/api/getdata', methods=['POST'])
def getdata():
    data = insert_data()
    return jsonify({"Table":data})


@app.route('/api/upload', methods=['POST'])
def upload_images():
    
    images = []
    normal, Nx2, micro = 0, 0,0

    for name, uploaded_image in request.files.items():
        if not uploaded_image.mimetype.startswith('image/'):
            continue

        with NamedTemporaryFile() as file:
            for b in iter(lambda: uploaded_image.read(cfg.chunk), b''):
                file.write(b)

            try:
                if (normal + Nx2+micro) > 1000:
                    break 
                
                p = predict(file.name)
                print(p)
                normal += p[0]
                Nx2 += p[1]
                micro+= p[2]
                
                images.append(p[3])
            except:
                traceback.print_exc()
                continue   
    pacient_id = data_entry(normal, Nx2,micro,((Nx2+micro)/normal)*100)
    return jsonify({'normal': normal, '2nucl': Nx2,'micro': micro, 'images': images, 'pacient_id':pacient_id})


@app.route('/api/image/<path:filename>')
def get_img_file(filename):
    p = Path(cfg.out_img_path) / Path(filename).name.strip('.')
    print(p)

    if not p.exists():
        return b'', 404

    @after_this_request
    def remove_file(resp):
        p.unlink()
        return resp

    return send_from_directory(cfg.out_img_path, filename)
