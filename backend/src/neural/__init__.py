import os
from tempfile import NamedTemporaryFile

import cv2
import keras
import keras.preprocessing.image
import numpy as np
import tensorflow as tf
from PIL import Image
from pathlib import Path

from .. import cfg
from .keras_retinanet.models.resnet import custom_objects
from .keras_retinanet.preprocessing.csv_generator import CSVGenerator
from .keras_retinanet.utils.image import read_image_bgr


def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
keras.backend.tensorflow_backend.set_session(get_session())
model = keras.models.load_model(
    cfg.keras_model_path, custom_objects=custom_objects)
labels = cfg.neural_labels
graph = tf.get_default_graph()

sred = 56.81220022067207 + 5


def predict(file):
    global graph
    with graph.as_default():
        normal = 0
        ubnormal = 0
        micro = 0

        with Image.open(file) as img:
            with NamedTemporaryFile() as tmp:
                img.thumbnail([520, 520])
                img.save(tmp.name, format='JPEG')
                image = read_image_bgr(tmp.name)

        draw = image.copy()
        draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

        annotation = np.array([0., 0., 0., 0., 0.])

        # process image
        _, _, detections = model.predict_on_batch(
            np.expand_dims(image, axis=0))

        predicted_labels = np.argmax(detections[0, :, 4:], axis=1)
        scores = detections[
            0, np.arange(detections.shape[1]), 4 + predicted_labels]

        for idx, (label, score) in enumerate(zip(predicted_labels, scores)):
            b = detections[0, idx, :4].astype(int)

            if abs(b[0] - b[2]) > sred:
                continue

            if score < 0.2:
                continue
            if score < 0.2 and label == 2:
                continue
            if score < 0.2 and label == 3:
                continue 

            if label == 1:
                normal += 1
            elif label == 2:
                ubnormal += 1
            elif label ==3:
                micro += 1 

            cv2.rectangle(draw, (b[0], b[1]), (b[2], b[3]), (0, 0, 255), 3)
            caption = "{} {:.3f}".format(labels[label], score)
            cv2.putText(draw, caption, (b[0], b[1] - 10),
                        cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 0), 3)
            cv2.putText(draw, caption, (b[0], b[1] - 10),
                        cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 255), 2)

        label = int(annotation[4])
        b = annotation[:4].astype(int)
        cv2.rectangle(draw, (b[0], b[1]), (b[2], b[3]), (0, 255, 0), 2)
        caption = "{}".format(labels[label])
        cv2.putText(draw, caption, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN,
                    1.5, (0, 0, 0), 3)
        cv2.putText(draw, caption, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN,
                    1.5, (255, 255, 255), 2)

        with NamedTemporaryFile(
                dir=cfg.out_img_path, delete=False,
                suffix='.jpg') as out_rect_img:
            img = Image.fromarray(draw)
            img.save(out_rect_img)

            return normal, ubnormal, micro, '/api/image/' + Path(out_rect_img.name).name
